---
title: "Fiduciary Licensing Agreement"
menu:
  main:
    parent: documents
    name: "FLA (Licensing)"
    weight: 3
---

At the General Assembly of KDE e.V. in August 2008
the membership voted to adopt a Fiduciary Licensing Agreement
as the preferred form for assigning copyright to KDE e.V.

Copyright assignment is a personal act. It is entirely *optional*
and at an individual developer, contributor or copyright holder's
discretion whether to assign copyright to KDE e.V. or not.
While there are various legal avenues available to
do such an assignment, a Fiduciary License Agreement
is one that preserves the spirit of Free Software
and is easy to administer.

* [Version 2.0](#version-20) is the **current** form of the FLA.
* [Version 1.3.5](#version-13) is the **older** form of the FLA, still available for use.

## Versions of the FLA

The text of the FLA and the relicensing document
has changed over time. Only the text of the particular
document you may have signed applies. Most changes are 
of minor importance, but they are documented here.

### Version 2.0

> Version 2.0 is the *current* FLA. New signatories are encouraged
> to use this one because of its up-to-date language.
> The FLA 2.0 is a single document without addenda or appendices.

* **For individuals**, the [FLA 2.0 form for individuals](/resources/FLA-2.0-individual.pdf) is available from
  the [resources](/resources) page.
* **For entities** such as companies, corporations, and cooperatives,
  the [FLA 2.0 form for entities](/resources/FLA-2.0-entity.pdf) should be used.

The Fiduciary Licensing Agreement 2.0 was obtained from the
[Contributor Agreements](https://contributoragreements.org/) website.

* **2.0** Initial version from the Contributor Agreements site.

### Version 1.3

> Versions prior to 1.3 were not used by KDE e.V. and there are no
> signatories of versions prior to 1.3. This version of the FLA is still
> available as an option, at your personal choice.

The forms ([FLA 1.3.5](/resources/FLA-1.3.5.pdf) and [FRP 1.3.5](/resources/FRP-1.3.5.pdf), PDF) for the Fiduciary Licensing Agreement
are available from the [resources](/resources)
page.

The Fiduciary Relicensing Policy (FRP) restricts the ways in which KDE e.V.
can relicense the code for which it holds the copyright.

The Fiduciary Licensing Agreement
was created in cooperation with the
[Free Software Foundation Europe](https://fsfe.org/activities/fla/fiduciary.en.html).

+ **1.3.5** Unified the prefab and non-prefab document.
+ **1.3.4** Updated post address of KDE e.V.
+ **1.3.3** Updated post address of KDE e.V.
+ **1.3.2** (with associated FRP version 1.3.1) Added FLA with boilerplate text for KDE SVN account holders.
+ **1.3.1** Clarify text and remove some licenses from FRP that we don't actually want to use.
+ **1.3** Clarify text by referring to specific version of KDE e.V. charter.
+ **1.2** Changes of address.
+ **1.2** Changes in the way the document is created.
+ **1.1** Initial version (the FSFE document is considered version 1.0).


## Signatories of the FLA

A lot of KDE contributors have signed the FLA. Among them are the following people 
alphabetically by first name (the numbers following the names indicate the version of the FLA that they have signed):

+ Aaron Seigo (1.3.1)
+ Adriaan de Groot (1.3.1)
+ Adrián Chaves Fernández (1.3.3)
+ Aitor Pazos Ibarzabal (1.3.3)
+ Àlex Fiestas (1.3.3)
+ Aleix Pol Gonzalez (1.3.3)
+ André Wöbbeking (1.3.3)
+ Andreas Cord-Landwehr (1.3.3)
+ Andreas Hartmetz (1.3.2)
+ Bhushan Shah (1.3.4)
+ Casper van Donderen (1.3.2)
+ Chani Armitage (1.3.2)
+ Christoph Cullmann (1.3.5)
+ Claus Christensen (1.3.4)
+ Cornelius Schumacher (1.3.1)
+ Cristian Tibirna (1.3.3)
+ Dan Leinir Turthra Jensen (1.3.3)
+ Daniel Laidig (1.3.3)
+ Daniel Vrátil (1.3.5)
+ David Edmundson (1.3.4)
+ Dominik Haumann (1.3.2)
+ Dominique Devriese (1.3.4)
+ Eckhart Wörner (1.3.2)
+ Elvis Angelaccio (1.3.5)
+ Franck Arrecot (1.3.4)
+ Frederik Gladhorn (1.3.4)
+ Harald Fernengel (1.3.3)
+ Ingo Klöcker (1.3.2)
+ Ivan Cukić (1.3.3)
+ John Layt (1.3.3)
+ Kevin Funk (1.3.4)
+ Kevin Krammer (1.3.3)
+ Kévin Ottens (1.3.4)
+ Luigi Toscano (1.3.3)
+ Lukas Sommer (1.3.3)
+ Lydia Pintscher (1.3.2)
+ Marco Martin (1.3.3)
+ Mark Gaiser (1.3.4)
+ Martin Gräßlin (1.3.3)
+ Matěj Laitl (1.3.4)
+ Matthias Ettrich (1.3.2)
+ Michael Bohlender (1.3.4)
+ Milian Wolff (1.3.2)
+ Miquel Canes Gonzalez (1.3.3)
+ Oswald Buddenhagen (1.3.2)
+ Pradeepto Bhattacharya (1.3.5)
+ Rafael Fernández López (1.3.2)
+ Rajeesh K V (1.3.4)
+ Reinhold Kainhofer (1.3.1)
+ Riccardo Iaconelli (1.3.3)
+ Rishab Arora (1.3.4)
+ Rohan Garg (1.3.3)
+ Sandro Knauß (1.3.4)
+ Scarlett Clark (1.3.4)
+ Sebastian Kügler (1.3.1)
+ Sébastien Renard (1.3.3)
+ Shantanu Tushar (1.3.4)
+ Simon Wächter (1.3.4)
+ Stefan Derkits (1.3.4)
+ Sune Vuorela (1.3.5)
+ Teo Mrnjavac (1.3.4)
+ Till Adam (1.3.2)
+ Torrie Fischer (1.3.3)
+ Valorie Zimmermann (1.3.4)
+ Vishesh Handa (1.3.3)
+ Volker Krause (1.3.5)
