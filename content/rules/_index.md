---
title: "Rules and Policies"
menu:
  main:
    parent: documents
    weight: 2
nosubpage: true
---

## Rules of Procedure

A couple of rules of procedure extend the articles of association. They are
decided by the memership by a vote.

+ [Supporting Members](/rules/supporting_members/)
+ [Online Voting](/rules/online_voting/)
+ [Rules of Procedures of KDE e.V. Board](/rules/rules_of_procedures_board/)

## Other Rules and Policies

Additional rules and policies handle specific areas where the KDE e.V. is
active. They are decided by the membership or the board.

+ [Fiduciary Licensing Agreement (FLA)](/rules/fla)
+ [Travel Cost Reimbursement Policy](/rules/reimbursement_policy/)
+ [KDE Sprint Policy](/rules/sprint_policy/)
+ [Thank You Policy](/rules/thankyou_policy/)

## Internal Policies

There are policies which govern internal workings of KDE e.V. or the board.

+ [Conflict of Interest Policy](ConflictofInterestPolicy.pdf)
