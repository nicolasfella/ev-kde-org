---
title: "Supporting Members"
menu:
  main:
    parent: organization
    weight: 4
description: Become a supporting member and help fund KDE activities
scssFiles:
- /css/main.scss
---

If your company or organization would like to become a supporting member please
have a look at the
[information how to become a supporting member of the KDE e.V.](/getinvolved/supporting-members/)

Individuals wishing to support KDE financially should consider to [Join the Game](https://relate.kde.org/civicrm/contribute/transact?reset=1&id=9).

Currently the supporting members of the KDE e.V. are:

<h2>Patrons of KDE
<img style="float:right;" src="/images/patron_small.png" class="clear" />
</h2>

<div class="logo-list img-patrons mb-5">
  <img src="https://kde.org/aether/media/patrons/canonical.svg" alt="Canonical" />
  <img src="https://kde.org/aether/media/patrons/google.svg" alt="Google" />
  <img src="https://kde.org/aether/media/patrons/suse.svg" alt="SUSE" />
  <img class="qt" alt="Qt Group" />
  <img src="https://kde.org/aether/media/patrons/blue-systems.svg" alt="Blue Systems" />
  <img src="https://kde.org/aether/media/patrons/slimbook.svg" alt="Slimbook" />
  <img src="https://kde.org/aether/media/patrons/tuxedo.png" alt="TUXEDO Computers" />
  <img src="https://kde.org/aether/media/patrons/kfocus.png" alt="Kubuntu Focus" />
  <img src="https://kde.org/aether/media/patrons/gnupg.svg" alt="g10 Code" />
</div>



<h2>Supporters of KDE
<img style="float:right;" src="/images/supporter_small.png" class="clear" />
</h2>

<div class="logo-list img-supporters">
  <img src="/images/supportingmembers/kdab.png" alt="KDAB" />
  <img src="/images/supportingmembers/basyskom.png" alt="Basyskom" />
  <img src="/images/supportingmembers/enioka.png" alt="Enioka Haute Couture" />
</div>
