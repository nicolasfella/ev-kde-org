---
title: "KDE e.V. Trusted IT Consulting Firms"
menu:
  main:
    parent: organization
    weight: 10
    name: Trusted IT Consulting Firms
---

<style>
.img-consultant {
  float: left;
  border: 1px solid #aaa;
  margin: 15px;
  margin-top: 7px;
  margin-left: 0;
  padding: 5px;
  border-radius: 3px;
}
</style>

In KDE we take pride in being a free software community and having an open,
free and fair development process.

However, we also understand that sometimes companies and institutions main
priority is not learning the processes of our community and they just want
issue solved or a feature implemented.
For this reason, we are offering here a list of consultants that have
expertise in dealing with the KDE Community and we know will help get your
project landed in KDE as best as possible.

We encourage you to contact the consultants directly, but if you feel we can help you,
do not hesitate to contact us at
<a href="mailto:consulting@kde.org">consulting@kde.org</a>.

The people currently (2023) handling mail to the consulting list are:
- Albert Astals Cid
- Aleix Pol Gonzalez
- Lydia Pintscher

## KDE Patrons

{{< consultants name="patrons" >}}

<h2 style="clear:both;">KDE Supporters</h2>

{{< consultants name="supporters" >}}

<h2 style="clear:both;">Other Consulting Firms</h2>

{{< consultants name="other" >}}

<h2 style="clear:both">Your company on this page</h2>

<p>We are more than happy of adding trusted consultants to this page. Please see
<a href="/consultants_join/">this page</a> for more information.

<script>
const consultants = document.querySelectorAll('.consultants');
consultants.forEach((consultant) => {
  for (var i = consultant.children.length; i >= 0; i--) {
    consultant.appendChild(consultant.children[Math.random() * i | 0]);
  }
});
</script>
