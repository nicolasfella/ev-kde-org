---
title: "KDE e.V. Affiliates"
menu:
  main:
    parent: organization
    weight: 7
    name: Affiliates
---

## Partners

The KDE e.V. partners with organizations around the world to advance its mission to support KDE.

+ KDE e.V. is an <a href="https://fsfeurope.org/associates/associates.en.html">Associate Organization of the FSF Europe</a>. KDE e.V. supports the <a href="https://fellowship.fsfe.org/">Fellowship of FSFE</a> as well, and many members of KDE e.V. are fellows as well.
+ KDE e.V. is an <a href="https://opensource.org/affiliates/list">affiliate of the Open Source Initiative (OSI)</a>.
+ KDE e.V. is a <a href="https://www.openinventionnetwork.com/licensees.php">member of the Open Invention Network (OIN) community of licensees</a>.
+ KDE e.V. is a <a href="https://www.oasis-open.org/">member of the OASIS society</a>.
+ KDE e.V. is a <a href="https://civicrm.org/providers/Software-f%C3%BCr-Engagierte-e.V.">member of theSoftware für Engagierte e.V.</a> where we support CiviCRM.

## Local organizations

The KDE e.V. has set up a local organization program to handle KDE activities and presence:

+ [KDE España](https://kde-espana.org)

## Community partners

+ <a href="/community-partners/">Here</a> you can find the communities that partner with KDE e.V. through the <a href="/activities/partnershipprogram/">partnership program</a>.
