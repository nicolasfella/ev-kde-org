---
title: 'KDE e.V. Quarterly Report 2007 Q3/Q4'
date: 2008-05-30 00:00:00 
layout: post
---

The <a href="http://ev.kde.org/reports/ev-quarterly-2007Q3-Q4.pdf">KDE e.V.
Quarterly Report</a> is now available for July to September, and October to December 2007. This document includes
reports of the board and the working groups about the KDE e.V. activities of the last two
quarters of 2007, and future plans.
