# KDE e.V. website

[![Build Status](https://binary-factory.kde.org/buildStatus/icon?job=Website_ev-kde-org)](https://binary-factory.kde.org/job/Website_ev-kde-org/)

This is the git repository for [ev.kde.org](https://ev.kde.org), the website for the KDE e.V..

As a (Hu)Go module, it requires both [Hugo](https://gohugo.io/) and Go to work.

## Development
Read [hugo-kde wiki](https://invent.kde.org/websites/hugo-kde/-/wikis/).

## I18n
See [hugoi18n](https://invent.kde.org/websites/hugo-i18n).
