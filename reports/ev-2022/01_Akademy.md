<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Akademy/lectern.jpg" alt="Akademy 2023 from the stage: lectern and teleprompter" />
    </figure>
</div>

[Akademy 2022](https://akademy.kde.org/2022) was held in Barcelona from the 1st to the 7th of October. The weekend of Saturday 1st of October and Sunday 2nd of October was dedicated to talks, panels and presentations. Community members and guests explained to the audience what had been going on within KDE's projects (and adjacent projects), the state of the art, and where things were headed.

### Day 1

At 10 o'clock sharp, Aleix Pol, President of KDE, opened this year's Akademy before an audience jittering with excitement. The attendees were animated with good reason: this was the first major in-person event for the KDE Community in two years. Old friends were seeing each other after a long time, and we were also meeting many new friends that we had only ever talked to [online](https://webchat.kde.org/).

Aleix remarked on how important this year's event was for the community, and especially for him, as Barcelona is Aleix's home town, and the Universitat Politécnica de Barcelona, the venue where activities were being held, his alma mater. After informing everyone about the logistics of the event, Aleix introduced the first keynote speakers: Volker Hilsheimer and Pedro Bessa from [Qt Company](https://qt-project.org/).

Volker and Pedro kicked off the talks proper with Volker introducing what is in store for us regarding Qt6.5 and beyond. Pedro laid out the new plans Qt Company has to upgrade the engagement with the Qt community, including KDE developers and contributors.

Then it was time to talk about [KDE's goals](https://community.kde.org/Goals), the projects that become a top priority for the community for two years. The goals program were started in the 2017, and the first three goals set in that year covered improving the usability and productivity for basic software, enhancing privacy in KDE software, and streamlining the onboarding of new contributors. The second set of goals started in 2019 and finished precisely at the 2022 Akademy. They covered making KDE's software and its components more visually consistent, promoting apps, and improving the implementation of Wayland in Plasma.

The goals voted on by the community in 2022 cover the areas of accessibility, sustainability and ways of making internal KDE processes and workflows more efficient.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Akademy/NewGoals_1500.jpg" alt="Adam presenting the new KDE Goals" />
    </figure>
</div>

After lunch, the talks were split into two tracks and, in track one Tomaz Canabrava discussed [Konsole](https://konsole.kde.org/), KDE's powerful terminal emulator, and what the future held for the project. Meanwhile, in room 2, Nate Graham covered his traditional *Konquering the World - Are we there yet?* in which he updates attendees on the progress KDE is making in tech markets and user adoption.

Later, Devin Lin and Bhushan Shah of [Plasma Mobile](https://plasma-mobile.org/) told the audience all about the progress KDE's mobile platform has made throughout the year. In room 2, a panel made up of Joseph De Veaugh-Geiss, Nicolas Fella (via videoconference), Karanjot Singh (in a pre-recorded video), and Lydia Pintscher informed the audience about the progress made by [KDE Eco](https://eco.kde.org/) and what the next steps for the project were.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Akademy/joseph_award.jpg" alt="Joseph De Veaugh-Geiss shows the eco-certification awarded to KDE's Okular PDF-document reader." />
        <br />
        <figcaption>Joseph De Veaugh-Geiss shows the eco-certification awarded to KDE's Okular PDF-document reader.</figcaption>
    </figure>
</div>

After a short break, David Edmundson took to the stage in room 1 and told us all about the success of the [Steam Deck](https://kde.org/hardware/), how the people at Valve were surprised at how popular KDE's Plasma desktop had become among users, and how they were using it in an unexpected numbers and ways. David also revealed that the Steam Deck had already sold more than a million units and was still going strong. In room 2, David Cahalane tackled the difficult issue of accessibility and explained how improving it for KDE Plasma and apps would help more users adopt KDE, not only because it would facilitate the usage of the software itself, but also because it would make the desktop and apps compliant with accessibility rules in public institutions and companies across the world.

Back in room 1, Aleix explained how KDE's [Plasma](https://kde.org/plasma-desktop/) had transcended the concept of desktop, as it was now moving into the territory of mobile and smart household appliances, like phones, cars and TVs. In Room 2, Harald Sitter talked about bugs and how frustrating it is when the system keeps crashing, how to identify the causes, and what tools were available to solve the issues.

For the final talks of the day, in room 1 Lina Ceballos of [FSFE](https://fsfe.org/) told us about the [Reuse](https://reuse.software/) project. The Reuse project intends to relieve much of the confusion and tediousness of licensing software online. At the same time in room 2, both Nicolas Fella and Alexander Lohnau remotely discussed getting applications ready for [KDE Frameworks 6](https://develop.kde.org/products/frameworks/). They discussed the current status of KF6 and why it is better to port now.

### Day 2

The following day, Hector Martín, the hacker that opened the Kinect, Play Station and Wii to the open source world, tuned in from Japan and told us about his new project in his keynote "[Asahi Linux](https://asahilinux.org/) - One chip, no docs, and lots of fun". Hector explained that the new M1 and M2 Macintosh machines built by Apple are made to run a variety of operating systems, but how the company does not provide any kind of indication on how that is done. Good job reverse engineering is what Hector and his team do best! Thank to their efforts, today Linux (and Plasma) can easily run on the new ARM-based machines.

Afterwards, first David Redondo, and later Aleix Pol, tackled the topic of Wayland in two different talks. Wayland is a hot topic for developers, since it will allow Plasma and KDE apps to evolve, improve their performance, and work more safely and reliably.

Following a caffeinated and baked goods respite, again the talks were split over two locations. In room 1, Nicolas Fella, live from his studio, explained what really happens when you launch an app; while, in room 2, Neal Gompa told us about how the [Fedora](https://getfedora.org/) distro implements Plasma on Fedora, the advantages of Kinoite and the future of Fedora and KDE on mobile.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Akademy/nicos_talk_1500.jpg" alt="Nicholas Fella gives his talk over video conference." />
        <br />
        <figcaption>Nicholas Fella joins the conference over a video link to explain what **really** happens when you launch an app.</figcaption>
    </figure>
</div>

Later on, Aditya Mehra ran us through [OpenVoiceOS](https://openvoiceos.com/), an operating system with a voice-enabled AI at its core. In room 2, Volker Krause explained how push notifications, used profusely in proprietary software, could be implemented using FLOSS.

After lunch, [KDE's Board](https://ev.kde.org/corporate/board/) sat down with attendees and presented their yearly report, informing the community about what work had been carried out and how resources had been used. This was followed by presentations prepared by each of the active working groups: the Advisory Board, the Community Working Group, the Financial Working Group, the Fundraising Working Group, the KDE Free Qt Working Group, and the Sysadmin Working Group.

While this was going on, Shyamnath Premnadh was presenting his talk on how C++ and Python can thrive together in room 2.

Following a brief coffee break, it was time for the lightning talks, and Volker Krause kicked things off by talking about what was happening with KDE Frameworks 6. Volker was followed by Lydia Pintscher, who spoke about the [new fundraisers](https://kdenlive.org/en/fund/) for specific projects. Later, Albert Astals presented the KDE Security team, and Harald Sitter gave us advice on how to remain healthy and sane, while writing healthy and sane code.

As the event drew to close, it was time to show appreciation for our sponsors and host. [Shells](https://shells.com/), [KDAB](https://www.kdab.com/), [Canonical](https://www.canonical.com/), [MBition](https://mbition.io/), [QT Company](https://www.qt.io/), the [Fedora Project](https://getfedora.org/), [Collabora](https://collabora.com/), [openSUSE](https://www.opensuse.org/), [Viking](https://www.vikingsoftware.com/), [Slimbook](https://kde.slimbook.es/), [Codethink](https://www.codethink.co.uk/), [syslinbit](https://www.syslinbit.com/), and [GitLab](https://www.gitlab.com/) took turns to explain their involvement with KDE and why they decided to support Akademy. [PINE64](https://pine64.org/) also received a round of applause for their support.

Finally, there was a round of applause for the Akademy Team, the members of the [Barcelona Free Software](https://bcnfs.org/) community, in particular Albert Astals, and all the other volunteers that organized the event and helped us enjoy our days with the KDE community in Barcelona.

The last act of the day was announcing the traditional Akademy awards. This year the award for the Best Application went to Jasem Mutlaq for his work on the phenomenal [KStars](https://apps.kde.org/kstars/) astronomical program. The Non-Application Contribution Award went to Harald Sitter for his work on debugging and improving KDE's code across the board. Finally, the Jury Award went to Aniqa Khokhar for her work setting up the KDE Network across the world.

Like the rest of the event, this part was a bit special, as the awardees of 2020 and 2021 joined the awardees of 2022 on the stage, as they had not had the chance to physically receive their awards before now.

And with that, the conference part of the event was officially closed and KDE community members prepared themselves for a week of BoFs, meetings and hacking sessions.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Akademy/groupphoto.jpg" alt="Akademy 2023 attendees" />
        <br />
        <figcaption>Attendees to KDE's 2022 Akademy conference.</figcaption>
    </figure>
</div>

If you missed it, all the talk recordings are now available on [PeerTube](https://tube.kockatoo.org/w/p/rNYtZ5yFrMrUEgNfz3AnLQ) and [YouTube](https://youtube.com/playlist?list=PLsHpGlwPdtMrhCT_3Wr_dqmwuo08YNZlZ), or, if you prefer you can also just grab the [files](https://files.kde.org/akademy/2022/).
