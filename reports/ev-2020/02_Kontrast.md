<figure class="image-right"> <img width="50%" src="images/Projects/kontrast.png" alt="Kontrast" /><figcaption>Kontrast.</figcaption></figure>

Meet [Kontrast](https://apps.kde.org/kontrast/), the new KDE program that is a contrast checker. Available for desktop and mobile devices, you can use Kontrast to choose background and text color combinations that work best for your website or app. Your users will find your colors easy to read and Kontrast will help improve the accessibility of your site or app for people with vision problems.

Kontrast won’t catch all the problems, but it should still be very helpful to catch any issues early on when designing your interface.

Another big feature of Kontrast is the possibility to generate random color combinations with good contrast. These colors can be saved in the application itself so that you can keep a particularly good color combination for later use.

Kontrast is available for the Linux desktop, [Plasma Mobile](https://www.plasma-mobile.org/) and there is also a Beta version for Android.

This application was built using the excellent [Kirigami framework](https://develop.kde.org/frameworks/kirigami/).