<figure class="image-right"> <img width="50%" src="images/Projects/neochat.png" alt="NeoChat" /><figcaption>NeoChat.</figcaption></figure>

Now KDE has its own Matrix app: [NeoChat](https://apps.kde.org/neochat/). NeoChat is written using our [Kirigami](https://develop.kde.org/frameworks/kirigami//) user interface framework which gives the app a modern look and feels and allows it to adapt well to all your devices, be it your desktop, laptop, tablet or phone.

Matrix is an instant messaging system similar to Whatsapp or Telegram, but uses an open and decentralized network for secure and privacy-protected communications. NeoChat is a visually attractive Matrix client that works on desktop computers and mobile phones.

### Convergence

NeoChat provides an elegant and convergent user interface, allowing it to adapt to any screen size automatically and gracefully.

This means you can use it both on desktop computers, where you might want to have a small bar on the side of your screen, but you can also enjoy NeoChat on [Plasma Mobile](https://www.plasma-mobile.org/) and Android phones.

NeoChat will be installed by default on the [PinePhone KDE edition](https://www.pine64.org/2020/12/01/kde-community-edition-is-now-available/) and we offer a nightly Android version too. The Android version is for the moment experimental and some features, like file uploading, don’t work yet.

### Features

NeoChat provides a timeline with support for simple messages and also allows you to upload images and video and audio files. You can reply to messages and add reactions.

NeoChat provides all the basic features the chat application needs: apart from sending and responding to messages, you can invite users to a room, start private chats, create new rooms and explore public rooms.

### Image Editor

NeoChat also includes a basic image editor that lets you crop and rotate images before sending them. The image editor is provided by a small library called KQuickImageEditor.

### Why Matrix and NeoChat?

Matrix is an open network for secure and decentralized communication. This is an initiative that is very much aligned with KDE’s goals of creating an open operating system for everybody. This is why we need a Matrix client that integrates into [Plasma](https://kde.org/plasma-desktop/) and thus NeoChat was born.

NeoChat is a fork of Spectral, another QML client, and uses the libQuotient library to interact with the Matrix protocol. It uses the Kirigami framework and QML to provide an elegant and convergent user interface.

### Translations

NeoChat is fully translated in English, Ukrainian, Swedish, Spanish, Portuguese, Hungarian, French, Dutch, Valencian, Catalan, British English, Italian, Norwegian Nynorsk and Slovenian.