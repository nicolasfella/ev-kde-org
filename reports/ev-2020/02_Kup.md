<figure class="image-right"> <img width="50%" src="images/Projects/kup.png" alt="Kup" /><figcaption>Kup.</figcaption></figure>

[Kup](https://apps.kde.org/kup/) helps people to Keep Up-to-date backups of their Personal files. You can use a USB hard drive to store files, but saving files to a server over a network connection is also possible for advanced users.

It was previously developed outside of KDE, but Kup has moved to be hosted by KDE and has now made its first release as a KDE app.

When you plug in your external hard drive Kup will automatically start copying your latest changes, but of course, it will only do so if you have been active on your computer for some hours since the last time you took a backup (and it can, of course, ask you first, before copying anything).

In general, Kup tries to not disturb you needlessly.

There are two types of backup schemes supported: one which keeps the backup folder completely in sync with what you have on your computer, deleting from the backup any file that you have deleted on your computer etc. The other scheme also keeps older versions of your files in the backup folder. When using this, only the small parts of your files that have changed since the last backup will be saved and, therefore, incremental backups are very fast. This is especially useful if you are working on big files. At the same time, it is as easy to access your files as if a complete backup was taken every time, as every backup contains a complete version of your directories. Behind the scenes, all the content that is the same is only stored once.

### Features

**Backup types:**

* Synchronized folders with the use of "rsync".
* Incremental backup archive with the use of "bup".

**Backup destinations:**

* Local filesystem path monitored for availability. That means you can set a destination folder which only exists when perhaps a eSATA harddrive or a network shared drive is mounted and Kup will detect when it becomes available.
* External storage, like USB hard drives. Also monitored for availability.

**Schedules:**

* Manual only (triggered from tray icon popup menu).
* Interval (suggests new backup after some time has passed since the last backup).
* Usage based (suggests new backup after you have been active on your computer for some hours since the last backup).